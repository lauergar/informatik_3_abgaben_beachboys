from abc import ABC, abstractmethod

class BorderBehavior(ABC):

    ''' This abstract function is overwritten in the inherited classes ZeroPaddingBorderBehavior and ClampingBorderBehavior. '''
    @abstractmethod
    def getPixelValue(self, i, j, image):
        pass

