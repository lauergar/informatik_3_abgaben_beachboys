from BorderBehavior import BorderBehavior


''' This function limits a value to a given range
    Source: https://www.pythonpool.com/python-clamp/ '''
def clamp(num, min_value, max_value):
    num = max(min(num, max_value), min_value)
    return num

class ClampingBorderBehavior(BorderBehavior):

    ''' This function returns the value of the image at the given index. If the index is out of range or negative it returns the value of the nearest pixel. '''
    def getPixelValue(self, i, j, image):

        try:
            if i < 0 or j < 0:
                clamped_i = clamp(i, 0, image.shape[0] - 1)
                clamped_j = clamp(j, 0, image.shape[1] - 1)
            else:

                return image[i][j]
        except IndexError:
            clamped_i = clamp(i, 0, image.shape[0]-1)
            clamped_j = clamp(j, 0, image.shape[1]-1)
        return image[clamped_i][clamped_j]


