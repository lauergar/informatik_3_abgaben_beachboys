from numpy import array


class Image:

    ''' Constructor to create an instance of Image. '''
    def __init__(self):
        self.__imageData = None

    ''' This function gets the imageData of the image. '''
    def get_imageData(self):
        return self.__imageData

    ''' This function sets the imageData to the given Value.  '''
    def set_imageData(self, value):
        self.__imageData = value
        return self.__imageData

    '''This function reads PGM image files and returns
    a numpy array. Image needs to have a P2 header number.
    Line1 stores the magic number, line2 the width and height of the array and line3 the maximum gray level.
    Comments are ignored. '''

    def readFromFile(self, filename):
        try:
            f = open(filename, "r")
        except OSError:
            print("Could not read file: ", filename)
            exit()

        count = 0
        while count < 3:
            line = f.readline()
            if line[0] == '#':
                continue
            count = count + 1
            if count == 1:
                magicNum = line.strip()
                try:
                    assert magicNum == "P2"
                except AssertionError:
                    print("Invalid PGM file! Wrong magic number.")
                    f.close()
                    exit()
            elif count == 2:
                size = (line.strip()).split()
                width = int(size[0])
                height = int(size[1])
            elif count == 3:
                maxVal = int(line.strip())
                try:
                    assert maxVal >= 0 and maxVal <= 255
                except AssertionError:
                    print("Invalid PGM file! Invalid Pixel value.")

        img = []
        values = f.read()
        elem = values.split()
        try:
            assert len(elem) == width * height
        except AssertionError:
            print("Error in number of pixels")
            exit()

        for i in range(height):
            temp = []
            for j in range(width):
                temp.append(elem[i * width + j])
            img.append(temp)
            Image.set_imageData(self, array(img))

        return self.__imageData

    ''' This function writes a numpy array to a PGM
      image file. By default, header number "P2" and max gray level "255" are 
      written. Width and height are same as the size of the given list. '''

    def writeToFile(self, filename):
        try:
            f = open(filename, "w+")
        except IOError:
            print("Cannot open file")
            exit()

        maxVal = 255
        width = 0
        height = 0
        for row in self.__imageData:
            height = height + 1
            width = len(row)

        f.write("P2" + "\n")
        f.write(str(width) + " " + str(height) + "\n")
        f.write(str(maxVal) + "\n")

        for i in range(height):
            for j in range(width):
                f.write(str(self.__imageData[i][j]) + " ")
            f.write("\n")

    ''' This function calculates and returns the new value of a single pixel.
        The central element of the filter matrix is located at the given index.'''

    def get_filtered_pixel(self, i, j, imageData, kernel, borderBehavior):
        kernel_half = int((len(kernel) - 1) / 2)
        final_pixel_value = 0

        for k in range(-kernel_half, kernel_half + 1):
            for l in range(-kernel_half, kernel_half + 1):
                kernel_factor = kernel[k + kernel_half][l + kernel_half]
                current_pixel = borderBehavior.getPixelValue(i + k, j + l, self.__imageData)
                current_pixel = int(current_pixel)
                final_pixel_value += current_pixel * kernel_factor
        return int(final_pixel_value)

    ''' This function calculates the convolved image with the given kernel filter and border behavior
        and returns the new convolved image.'''

    def convolve(self, kernel, borderBehavior):

        rows = len(self.__imageData)
        cols = len(self.__imageData[0])

        for i in range(rows):
            for j in range(cols):
                self.__imageData[i][j] = Image.get_filtered_pixel(self, i, j, self.__imageData, kernel, borderBehavior)


        return self.__imageData
