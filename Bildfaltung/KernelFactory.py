class KernelFactory:

    ''' This static function returns a fix 3x3 matrix (Vertical Prewitt Kernel) '''
    @staticmethod
    def createVerticalPrewittKernel():
        verticalPrewittKernel = [[-1, -1, -1],
                                 [0, 0, 0],
                                 [1, 1, 1]]
        return verticalPrewittKernel

    ''' This static function returns a fix 3*3 matrix (Horizontal Prewitt Kernel) '''
    @staticmethod
    def createHorizontalPrewittKernel():
        horizontalPrewittKernel = [[-1, 0, 1],
                                   [-1, 0, 1],
                                   [-1, 0, 1]]
        return horizontalPrewittKernel

    ''' This static function returns a n*n sized Matrix depending on the given size. Size has to be odd. '''
    @staticmethod
    def createBoxFilter(size):
        if size % 2 != 0:
            value = 1/(size*size)
            boxFilter = [[value for x in range(size)] for y in range(size)]

            return boxFilter

        else:
            return("Invalid boxfilter size!")



