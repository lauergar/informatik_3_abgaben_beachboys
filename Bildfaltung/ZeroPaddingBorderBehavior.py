from BorderBehavior import BorderBehavior


class ZeroPaddingBorderBehavior(BorderBehavior):

    ''' This function returns the value of the image at the given index. If the index is out of range or negative it returns 0 instead. '''
    def getPixelValue(self, i, j, image):
        if i < 0 or j < 0:
            return 0
        try:
            return image[i][j]
        except IndexError:
            return 0





