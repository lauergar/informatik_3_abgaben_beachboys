from Image import Image


from KernelFactory import KernelFactory
from ZeroPaddingBorderBehavior import ZeroPaddingBorderBehavior
from ClampingBorderBehavior import ClampingBorderBehavior





pathImg1 = ".//images//Bild 1.pgm"
pathImg2 = ".//images//Bild 2.pgm"





'''Main for showing the functionalities of the implementation'''

def main():
    clamping = ClampingBorderBehavior()
    zero = ZeroPaddingBorderBehavior()
    horizontal = KernelFactory.createHorizontalPrewittKernel()
    vertical = KernelFactory.createVerticalPrewittKernel()
    box3 = KernelFactory.createBoxFilter(3)
    box11 = KernelFactory.createBoxFilter(11)
    box27 = KernelFactory.createBoxFilter(27)

    img1_horizontal = Image()
    img1_horizontal.readFromFile(pathImg1)
    img1_horizontal.convolve(horizontal, zero)
    img1_horizontal.writeToFile(".//images//ZeroPadded//Ergebnis 1_horizontal.pgm")

    img2_horizontal = Image()
    img2_horizontal.readFromFile(pathImg1)
    img2_horizontal.convolve(horizontal, clamping)
    img2_horizontal.writeToFile(".//images//Clamped//Ergebnis 1_horizontal.pgm")

    img1_vertical = Image()
    img1_vertical.readFromFile(pathImg1)
    img1_vertical.convolve(vertical, zero)
    img1_horizontal.writeToFile(".//images//ZeroPadded//Ergebnis 1_vertical.pgm")

    img2_vertical = Image()
    img2_vertical.readFromFile(pathImg1)
    img2_vertical.convolve(vertical, clamping)
    img2_horizontal.writeToFile(".//images//Clamped//Ergebnis 1_vertical.pgm")

    img3_box3_zero = Image()
    img3_box3_zero.readFromFile(pathImg2)
    img3_box3_zero.convolve(box3, zero)
    img3_box3_zero.writeToFile(".//images//ZeroPadded//Ergebnis 2_5.pgm")

    img3_box3_clamp = Image()
    img3_box3_clamp.readFromFile(pathImg2)
    img3_box3_clamp.convolve(box3, clamping)
    img3_box3_clamp.writeToFile(".//images//Clamped//Ergebnis 2_5.pgm")

    img4_box11_zero = Image()
    img4_box11_zero.readFromFile(pathImg2)
    img4_box11_zero.convolve(box11, zero)
    img4_box11_zero.writeToFile(".//images//ZeroPadded//Ergebnis 2_11.pgm")

    img4_box11_clamp = Image()
    img4_box11_clamp.readFromFile(pathImg2)
    img4_box11_clamp.convolve(box11, clamping)
    img4_box11_clamp.writeToFile(".//images//Clamped//Ergebnis 2_11.pgm")

    img5_box27_zero = Image()
    img5_box27_zero.readFromFile(pathImg2)
    img5_box27_zero.convolve(box27, zero)
    img5_box27_zero.writeToFile(".//images//ZeroPadded//Ergebnis 2_27.pgm")

    img5_box27_clamp = Image()
    img5_box27_clamp.readFromFile(pathImg2)
    img5_box27_clamp.convolve(box27, clamping)
    img5_box27_clamp.writeToFile(".//images//Clamped//Ergebnis 2_27.pgm")

if __name__ == '__main__':
    main()






























