import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;


/**
 * A generic and atomically safe Buffer data structure for holding values important in multithreaded use cases.
 * @param <T> The type of item to hold within the Buffer
 */
public class Buffer<T> {
    AtomicReferenceArray<T> items;
    AtomicInteger itemCount;
    int size;

    /**
     * Constructor for Buffer data structure
     * @param size The size of the Buffer
     */
    public Buffer(int size) {
        this.items = new AtomicReferenceArray<>(size);
        this.itemCount = new AtomicInteger(0);
        this.size = size;

        assert this.size >= 0 : "Size must be greater than 0";
    }

    /**
     * Push an item to the Buffer
     * @param item The item to be pushed
     * @throws IllegalStateException Thrown if pushing would exceed the maximum allowed size
     */
    public void push(T item) throws IllegalStateException {

        if (this.itemCount.get() >= this.size) {
            throw new IllegalStateException();
        }



        this.items.set(this.itemCount.get(), item);
        this.itemCount.incrementAndGet();
    }

    /**
     * Pop an item from the Buffer
     * @return The item to be popped
     *
     * @throws IllegalStateException Thrown if popping would underflow the minimum allowed size
     */
    public T pop() throws IllegalStateException {
        if (this.itemCount.get() <= 0) {
            throw new IllegalStateException();
        }

        return this.items.getAndSet(this.itemCount.getAndDecrement(), null);
    }

    /**
     * Check whether the Buffer is full
     * @return True if the Buffer is full, False otherwise
     */
    public boolean full() {
        return this.itemCount.get() >= this.size;
    }

    /**
     * Check whether the Buffer is empty
     * @return True if the Buffer is empty, False otherwise
     */
    public boolean empty() {
        return Integer.valueOf(this.itemCount.get()).equals(0);
    }
}
