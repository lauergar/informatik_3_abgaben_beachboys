import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Consumer implements Runnable {
    private final Buffer<Car> buffer;
    private final int interval;
    private final AtomicBoolean asleep;
    private final AtomicBoolean wakeCall;

    /**
     * Constructor for Consumer data structure
     * @param buffer Buffer instance for Consumer
     * @param interval interval of Consumer
     * @param asleep Boolean for Status
     * @param wakeCall Boolean for Status
     */
    /**
     "@Requires"({"buffer!=null","interval > 0","asleep!=null","wakeCall!=null", })
     */
    public Consumer(Buffer<Car> buffer, int interval, AtomicBoolean asleep, AtomicBoolean wakeCall) {
        this.buffer = buffer;
        this.interval = interval;
        this.asleep = asleep;
        this.wakeCall = wakeCall;
    }

    /**
     * Run function for the Consumer
     * This Function checks randomly if the buffer has Workitems and Pops them if that is the case.
     * If the buffer is full it wakens the sleeping consumers.
     *
     */
    @Override
    public void run() {
        while(true) {
            try {
                TimeUnit.SECONDS.sleep((int)(Math.random() * this.interval + 1));

                boolean wasFull = this.buffer.full();

                if (!this.asleep.get()) {
                    System.out.printf("%s consumer ...%n", Thread.currentThread().getId());

                    if (!this.buffer.empty()) {
                        this.buffer.pop();
                        System.out.printf(
                                "%s consumer TAKING OUT %s/%s %n",
                                Thread.currentThread().getId(),
                                this.buffer.itemCount,
                                this.buffer.size
                        );
                    } else {
                        this.asleep.set(true);
                    }
                }

                if (wasFull) {
                    this.wakeCall.set(false);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
