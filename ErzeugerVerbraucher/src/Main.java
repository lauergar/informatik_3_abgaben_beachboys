import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Main to show the functionalities of this Implementation
 */
public class Main {
    public static void main(String[] args) {
        Buffer<Car> buffer = new Buffer<>(5);
        AtomicBoolean consumerStatus = new AtomicBoolean(false);
        AtomicBoolean producerStatus = new AtomicBoolean(false);
        int consumerCount = Integer.parseInt(args[0]);
        int producerCount = Integer.parseInt(args[1]);

        for (int i = 0; i < consumerCount; i++) {
            Thread consumerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Consumer consumer = new Consumer(buffer, 15, consumerStatus, producerStatus);
                    consumer.run();
                }
            });

            consumerThread.start();
        }

        for (int i = 0; i < producerCount; i++) {
            Thread producerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Producer producer = new Producer(buffer, 15, producerStatus, consumerStatus);
                    producer.run();
                }
            });

            producerThread.start();
        }
    }
}