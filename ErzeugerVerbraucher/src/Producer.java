import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Producer implements Runnable {
    private final Buffer<Car> buffer;
    private final int interval;
    private final AtomicBoolean asleep;
    private final AtomicBoolean wakeCall;

    /**
     * Constructor for Consumer data structure
     * @param buffer Buffer instance for Producer
     * @param interval interval of Consumer
     * @param asleep Boolean for Status
     * @param wakeCall Boolean for Status
     */
    public Producer(Buffer<Car> buffer, int interval, AtomicBoolean asleep, AtomicBoolean wakeCall) {
        this.buffer = buffer;
        this.interval = interval;
        this.asleep = asleep;
        this.wakeCall = wakeCall;
    }

    /**
     * Run Function for the Producer
     * This function Produces Workitems randomly when the Buffer is not full.
     * If the Buffer is empty it wakens the Sleeping Producers
     */
    @Override
    public void run() {
        while(true) {
            try {
                TimeUnit.SECONDS.sleep((int)(Math.random() * this.interval + 1));

                boolean wasEmpty = this.buffer.empty();

                if (!this.asleep.get()) {
                    System.out.printf("%s producer ...%n", Thread.currentThread().getId());

                    if (!this.buffer.full()) {
                        this.buffer.push(new Car());
                        System.out.printf(
                                "%s producer PUSHING IN %s/%s %n",
                                Thread.currentThread().getId(),
                                this.buffer.itemCount,
                                this.buffer.size
                        );
                    } else {
                        this.asleep.set(true);
                    }
                }

                if (wasEmpty) {
                    this.wakeCall.set(false);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
