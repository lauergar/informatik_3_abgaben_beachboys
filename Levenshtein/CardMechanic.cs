﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    /// <summary>
    /// Class to read from scrambled.txt and referenced.txt into lists, repair cards and write repaired cards into fixed.txt.
    /// </summary>
    public class CardMechanic
    {
        const string pathScrambled = "../../../textdocs/scrambled.txt";
        const string pathReference = "../../../textdocs/reference.txt";
        const string pathFixed = "../../../textdocs/fixed.txt";
        
        private List<Cards> _cards = new List<Cards>();
        private List<string> _references = new List<string>();
        private List<Cards> _fixedCards = new List<Cards>();
        
        /// <summary>
        /// Method to read card information from scrambled.txt and storing them a in list.
        /// </summary>
        public void ReadFromScrambled()
            
        {
            var read = System.IO.File.ReadLines(pathScrambled);
            foreach (string line in read)
            {

                var part = line.Split('|');
                var card = new Cards()
                {
                    Name = part[0],
                    Mana = part[1],
                    Cmc = double.Parse(part[2]),
                    Type = part[3],
                    Count = double.Parse(part[4]),
                };
                _cards.Add(card);
                
            }
            
        }
        
        
        /// <summary>
        /// Method to store card information from a list in fixed.txt
        /// </summary>
        public void CardsToFile(List<Cards> l)
        {
            using (StreamWriter writer = new StreamWriter(pathFixed))
            {
                foreach (Cards card in l)
                {
                    writer.WriteLine(card.Name + "|" + card.Mana + "|" + card.Cmc + "|" + card.Type + "|" + card.Count);
                }
            }
            
        }
        
        /// <summary>
        /// Method to read card reference names from reference.txt and storing them a in list
        /// </summary>
        public void ReadFromReference()
        {
            var read = System.IO.File.ReadLines(pathReference);
            foreach (string line in read)
            {
                _references.Add(line);
            }
        }
        
        /// <summary>
        /// Method to restore damaged by calculating Levenshtein Distance between the card's name und each reference name.
        /// If the Levenshtein Distance is less than 26,75% of the card's name length the card name will be replaced by the reference name and stored in a list.
        /// Restored cards are written in fixed.txt.
        /// </summary>
        public void RestoreCards()
        {
            
            for (var i = 0; i < _cards.Count; i++)
            {
                foreach (string reference in _references)
                {
                    Levenshtein l = new Levenshtein(_cards[i].Name, reference);
                    if ((l.LevenshteinDistance() < (reference.Length * 0.2675)))
                    {
                        _cards[i].Name = reference;
                        _fixedCards.Add(_cards[i]);
                    }
                }
            }
            CardsToFile(_fixedCards);
        }
    }
    
}
