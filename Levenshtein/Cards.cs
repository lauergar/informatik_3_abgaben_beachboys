﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    /// <summary>
    /// Data structure for holding the card's information
    /// </summary>
    public class Cards
    {   /// <summary>
        /// Gets and sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets and inits the Mana
        /// </summary>
        public string Mana { get; init; }
        /// <summary>
        /// Gets and inits the cumulative mana costs
        /// </summary>
        public double Cmc { get; init; }
        /// <summary>
        /// Gets and inits the Type
        /// </summary>
        public string Type { get; init; }
        /// <summary>
        /// Gets and inits the Count
        /// </summary>
        public double Count { get; init; }

    }

}
