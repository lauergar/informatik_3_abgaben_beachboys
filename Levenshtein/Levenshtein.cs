﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    /// <summary>
    /// Class which implements the Levensthein algorithm.
    /// </summary>
    public class Levenshtein
    {
        private string _s;
        private string _t;
        private int[,] _matrix;

        /// <summary>
        /// Constructor to create a Instance of Levenshtein with two strings.
        /// </summary>
        /// <param name="s1">First string of the Levenshtein matrix</param>
        /// <param name="s2">Second string of the Levenshtein matrix</param>
        public Levenshtein(string s1, string s2)
        {
            _s = s1;
            _t = s2;
            _matrix = new int[_s.Length + 1, _t.Length + 1];

            for (int i = 0; i < _matrix.GetLength(1); i++)
            {
                _matrix[0, i] = i;
            }

            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                _matrix[i, 0] = i;
            }
        }
        /// <summary>
        /// Calculates the editing distance between the two strings set while initialization of the Levenshtein obeject.
        /// </summary>
        /// <returns>The calculated Levenshtein distance between the two given strings</returns>
        public int LevenshteinDistance()
        {
            for (int i = 1; i < _matrix.GetLength(0); i++)
            {
                for (int j = 1; j < _matrix.GetLength(1); j++)
                {
                    int c = (_s[i - 1] == _t[j - 1]) ? 0 : 1;
                    int rep = _matrix[i - 1, j - 1] + c;
                    int ins = _matrix[i, j - 1] + 1;
                    int del = _matrix[i - 1, j] + 1;

                    _matrix[i, j] = Math.Min(rep, Math.Min(ins, del));

                }
            }

            int distance = _matrix[_matrix.GetLength(0) - 1, _matrix.GetLength(1) - 1];

            return distance;
        }
        /// <summary>
        /// Prints the Levenshtein matrix
        /// </summary>
        public void PrintMatrix()
        {
            for (var i = 0; i < _matrix.GetLength(0); i++)
            {
                for (var j = 0; j < _matrix.GetLength(1); j++)
                {
                    Console.Write(_matrix[i, j] + " ");
                }
                Console.WriteLine();
            }

        }

    }
}

