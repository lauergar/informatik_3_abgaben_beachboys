﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{   /// <summary>
    /// Main class for showing the functionalities of the implementation
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main function to show functionality by creating a CardMechanic object reading from scrambled.txt and reference.txt to repair damaged cards
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            
            CardMechanic obj = new CardMechanic();
            obj.ReadFromScrambled();
            obj.ReadFromReference();
            obj.RestoreCards();

        }

    }
}

